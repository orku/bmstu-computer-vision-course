#include "Matrix.h"

#include <iomanip>
#include <limits>
#include <ostream>

namespace hw_cv {
    Matrix::Matrix(unsigned int rows, unsigned int cols) {
        if (rows == 0 || cols == 0) {
            throw std::invalid_argument("Null size matrix");
        }
        rows_ = rows;
        cols_ = cols;
        data_.resize(rows_);
        for (auto& row : data_) {
            row.resize(cols_);
        }
    }

    double Matrix::operator()(unsigned int i, unsigned int j) const {
        if (i >= rows_ || j >= cols_) {
            throw std::invalid_argument("Incorrect element index");
        }
        return data_[i][j];
    }

    double& Matrix::operator()(unsigned int i, unsigned int j) {
        if (i >= rows_ || j >= cols_) {
            throw std::invalid_argument("Incorrect element index");
        }
        return data_[i][j];
    }

    Matrix::Matrix(const std::vector<std::vector<double>>& mat) {
        if (mat.empty()) {
            throw std::invalid_argument("Empty matrix");
        }
        if (mat[0].empty()) {
            throw std::invalid_argument("Empty row");
        }
        rows_ = mat.size();
        cols_ = mat[0].size();
        data_ = mat;
    }

    Matrix Matrix::operator*(double val) const {
        Matrix new_mat(rows_, cols_);
        for (size_t i = 0; i < rows_; ++i) {
            for (size_t j = 0; j < cols_; ++j) {
                new_mat(i, j) = (*this)(i, j) * val;
            }
        }
        return new_mat;
    }

    Matrix Matrix::T() const {
        Matrix t_mat(cols_, rows_);

        for (size_t i = 0; i < t_mat.rows(); ++i) {
            for (size_t j = 0; j < t_mat.cols(); ++j) {
                t_mat(i, j) = (*this)(j, i);
            }
        }
        return t_mat;
    }

    bool Matrix::operator==(const Matrix& mat) const {
        if (rows_ != mat.rows() || cols_ != mat.cols()) {
            return false;
        }
        static constexpr double EPS = 1e-7;
        for (size_t i = 0; i < mat.rows(); ++i) {
            for (size_t j = 0; j < mat.cols(); ++j) {
                if (std::abs(mat(i, j) - (*this)(i, j)) > EPS) {
                    return false;
                }
            }
        }

        return true;
    }

    bool Matrix::operator!=(const Matrix& mat) const { return !(*this == mat); }

    Matrix Matrix::dot(const Matrix& mat) const {
        if (cols_ != mat.rows()) {
            throw std::invalid_argument("Incorrect sizes of matrix");
        }

        Matrix matrix(rows_, mat.cols());

        for (size_t i = 0; i < matrix.rows(); ++i) {
            for (size_t j = 0; j < matrix.cols(); ++j) {
                for (size_t k = 0; k < cols_; ++k) {
                    matrix(i, j) += (*this)(i, k) * mat(k, j);
                }
            }
        }
        return matrix;
    }

    Matrix Matrix::operator+(const Matrix& mat) const {
        if (rows_ != mat.rows() || cols_ != mat.cols()) {
            throw std::invalid_argument("Incorrect sizes of matrix");
        }

        Matrix matrix(rows_, cols_);

        for (size_t i = 0; i < matrix.rows(); ++i) {
            for (size_t j = 0; j < matrix.cols(); ++j) {
                matrix(i, j) = (*this)(i, j) + mat(i, j);
            }
        }
        return matrix;
    }

    Matrix Matrix::operator-(const Matrix& mat) const { return *this + (-mat); }

    Matrix Matrix::operator-() const {
        Matrix matrix(rows_, cols_);

        for (size_t i = 0; i < matrix.rows(); ++i) {
            for (size_t j = 0; j < matrix.cols(); ++j) {
                matrix(i, j) = -(*this)(i, j);
            }
        }
        return matrix;
    }

    double Matrix::det() const {
        if (rows_ != cols_) {
            throw std::invalid_argument("Matrix is not square");
        }
        auto [L, U] = LU();

        double det = (*this)(0, 0);
        for (unsigned int i = 1; i < rows_; ++i) {
            det *= U(i, i);
        }

        return det;
    }

    std::pair<Matrix, Matrix> Matrix::LU() const {
        Matrix L(rows_, cols_);
        Matrix U(rows_, cols_);
        for (unsigned int i = 0; i < rows_; ++i) {
            for (unsigned int j = 0; j < cols_; j++) {
                if (j < i) {
                    U(i, j) = 0;
                } else {
                    U(i, j) = (*this)(i, j);
                    for (unsigned int k = 0; k < i; k++) {
                        U(i, j) -= L(i, k) * U(k, j);
                    }
                }
            }
            for (unsigned int j = 0; j < cols_; ++j) {
                if (j < i) {
                    L(j, i) = 0;
                } else if (j == i) {
                    L(i, j) = 1;
                } else {
                    L(j, i) = (*this)(j, i) / U(i, i);
                    for (unsigned int k = 0; k < i; ++k) {
                        L(j, i) -= L(j, k) * U(k, i) / U(i, i);
                    }
                }
            }
        }
        return {L, U};
    }

    Matrix Matrix::inverse() const {
        Matrix IA(rows_, cols_);
        for (unsigned int i = 0; i < rows_; ++i) {
            IA(i, i) = 1;
        }
        auto [L, U] = LU();
        for (int j = 0; j < rows_; j++) {
            for (int i = 0; i < cols_; i++) {
                for (int k = 0; k < i; k++) {
                    IA(i, j) -= L(i, k) * IA(k, j);
                }
                IA(i, j) /= L(i, i);
            }

            for (int i = rows_ - 1; i >= 0; i--) {
                for (int k = i + 1; k < rows_; k++) {
                    IA(i, j) -= U(i, k) * IA(k, j);
                }

                IA(i, j) /= U(i, i);
            }
        }
        return IA;
    }

    std::ostream& operator<<(std::ostream& out, const Matrix& matrix) {
        out.exceptions(std::ostream::failbit | std::ostream::badbit);

        out << std::setprecision(std::numeric_limits<double>::max_digits10);

        unsigned int cols = matrix.cols();
        for (size_t i = 0; i < matrix.rows(); ++i) {
            for (size_t j = 0; j < cols; ++j) {
                out << matrix(i, j);
                if (j == cols - 1) {
                    out << "\n";
                } else {
                    out << " ";
                }
            }
        }
        return out;
    }

    Matrix operator*(double val, const Matrix& matrix) { return matrix * val; }

    Matrix forward(const Matrix& L, const Matrix& b) {
        Matrix y(L.rows(), 1);

        for (int i = 0; i < L.rows(); ++i) {
            y(i, 0) = b(i, 0);
            for (int j = 0; j < i; ++j) {
                y(i, 0) -= L(i, j) * y(j, 0);
            }
            y(i, 0) /= L(i, i);
        }
        return y;
    }

    Matrix back(const Matrix& U, const Matrix& y) {
        Matrix x(U.rows(), 1);

        for (int i = U.rows() - 1; i >= 0; --i) {
            x(i, 0) = y(i, 0);
            for (int j = i + 1; j < U.rows(); ++j) {
                x(i, 0) -= U(i, j) * x(j, 0);
            }
            x(i, 0) /= U(i, i);
        }
        return x;
    }

    Matrix solve(const Matrix& A, const Matrix& b) {
        auto [L, U] = A.LU();

        Matrix y = forward(L, b);
        Matrix x = back(U, y);
        return x;
    }

}
