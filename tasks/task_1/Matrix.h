#pragma once

#include <string>
#include <vector>

namespace hw_cv {
    class Matrix {
    public:
        Matrix() = delete;
        Matrix(uint32_t rows, uint32_t cols);
        explicit Matrix(const std::vector<std::vector<double>>& mat);
        Matrix(const Matrix& mat) = default;
        Matrix& operator=(const Matrix& mat) = default;
        Matrix(Matrix&& mat) = default;

        ~Matrix() = default;

        double operator()(unsigned int i, unsigned int j) const;
        double& operator()(unsigned int i, unsigned int j);
        [[nodiscard]] double det() const;
        [[nodiscard]] Matrix dot(const Matrix& mat) const;
        [[nodiscard]] Matrix T() const;
        [[nodiscard]] std::pair<Matrix, Matrix> LU() const;
        [[nodiscard]] Matrix inverse() const;


        Matrix operator*(double val) const;
        Matrix operator+(const Matrix& mat) const;
        Matrix operator-(const Matrix& mat) const;
        Matrix operator-() const;

        bool operator==(const Matrix& mat) const;
        bool operator!=(const Matrix& mat) const;

        [[nodiscard]] inline unsigned int rows() const{
            return rows_;
        };

        [[nodiscard]] inline unsigned int cols() const{
            return cols_;
        };

    private:
        unsigned int rows_;
        unsigned int cols_;
        std::vector<std::vector<double>> data_;
    };

    Matrix operator*(double val, const Matrix& matrix);
    std::ostream& operator<<(std::ostream& os, const Matrix& matrix);

    Matrix solve(const Matrix& A, const Matrix& b);

}
