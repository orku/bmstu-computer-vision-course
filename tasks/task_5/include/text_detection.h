#pragma once

#include <string>
#include <experimental/filesystem>

#include <opencv2/opencv.hpp>

namespace hw_cv {
  void detect_text(const std::string &path_to_pic);
}