#include "text_detection.h"

int main() {
  std::string PATH_TO_PIC = __FILE__;
  PATH_TO_PIC = PATH_TO_PIC.substr(0, PATH_TO_PIC.rfind('/'));
  PATH_TO_PIC += "/data/test_marker.jpg";

  hw_cv::detect_text(PATH_TO_PIC);

  return 0;
}