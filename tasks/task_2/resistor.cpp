#include <fstream>
#include <iostream>
#include <filesystem>

#include <Eigen/Core>
#include <Eigen/LU>


template<typename M>
M load_csv(const std::string &path) {
    std::ifstream file(path);
    std::string line;
    std::vector<double> values;
    uint rows = 0;
    // skip headers
    std::getline(file, line);
    // read data
    while (std::getline(file, line)) {
        std::stringstream lineStream(line);
        std::string cell;
        while (std::getline(lineStream, cell, ' ')) {
            values.push_back(std::stod(cell));
        }
        ++rows;
    }
    return Eigen::Map<const Eigen::Matrix<typename M::Scalar, M::RowsAtCompileTime, M::ColsAtCompileTime, Eigen::RowMajor>>(
            values.data(), rows, values.size() / rows);
}

int main() {
    std::string FILEPATH = __FILE__;

    FILEPATH = FILEPATH.substr(0, FILEPATH.rfind('/')) +   "/data.csv";
    auto data = load_csv<Eigen::MatrixXd>(FILEPATH);

    Eigen::MatrixXd R = data.col(0);
    for (long i = 0; i < R.rows(); ++i) {
        R(i, 0) = std::log(data(i, 0));
    }
    Eigen::MatrixXd T(data.rows(), 2);
    // append ones col for matrix
    for (long i = 0; i < T.rows(); ++i) {
        T(i, 0) = data(i, 1);
        T(i, 1) = 1;
    }

    auto T_2 = T.transpose() * T;
    auto T_R = T.transpose() * R;

    double k1 =  T_2.lu().solve(T_R)(0, 0);
    double k2 =  T_2.lu().solve(T_R)(1, 0);

    while (true){
        std::cout << "Type the temperature value to compute resistance or type 1000 to stop calculation:" << std::endl;
        double temp = 0.0;
        std::cin >> temp;
        if (temp > 999)
            break;
        std::cout << "The resistance is: " << exp(temp * k1 + k2) <<  std::endl;
    }
}