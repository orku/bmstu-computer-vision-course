#include "feature_matching.h"


int main(){
  std::string PATH_TO_VIDEO = __FILE__;
  PATH_TO_VIDEO = PATH_TO_VIDEO.substr(0, PATH_TO_VIDEO.rfind('/'));
  PATH_TO_VIDEO += "/data/sample_mpg.avi";


  auto using_adaptive_alignment = [](){
    std::cout << "Использовать адаптивное выравнивание?" << std::endl
              << "1 - Да" << std::endl
              << "2 - Нет" << std::endl;
    int use = 0;
    std::cin >> use;

    if(use == 1) {
      return true;
    } else if(use == 2) {
      return false;
    } else {
      std::cout << "Выравнивание не используется" << std::endl;
      return false;
    }
  };

  hw_cv::feature_matching(PATH_TO_VIDEO, using_adaptive_alignment());

  return 0;
}