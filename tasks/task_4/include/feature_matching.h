#pragma once

#include <tuple>
#include <vector>
#include <string>

#include <opencv2/opencv.hpp>
#include <opencv2/features2d.hpp>

namespace hw_cv {
  void feature_matching(const std::string &path, bool using_adaptive_alignment = false);
}
