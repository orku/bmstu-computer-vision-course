#pragma once

#include <Eigen/Core>
#include <functional>

namespace hw_cv {
    using RecalcFuncType = std::function<Eigen::VectorXd(double, const Eigen::VectorXd &)>;
    using ResultFuncType = std::function<Eigen::VectorXd(double)>;

    constexpr double DEFAULT_STEP = 0.1;
    constexpr double DIFF_STEP = 0.001;

    struct SolverOptions {
        double step;

        double max_diff;
        double min_diff;

        double start_time;

        Eigen::VectorXd start_values;
    };

    class Solver {
    public:
        virtual ~Solver() = default;

        virtual void solve() = 0;
    };

    class RungeSolver : public Solver {
    public:
        struct Result {
            double max_diff;
            double step;
        };

        RungeSolver(RecalcFuncType &recalc_func, ResultFuncType &result_func, const SolverOptions &opt)
                : recalc_func_(recalc_func), result_func_(result_func), time_(opt.start_time), step_(opt.step),
                  values_(opt.start_values), max_diff_(opt.max_diff), min_diff_(opt.min_diff) {
            init_butcher_();
        };

        void solve() override;

        Result result() { return res_; };

    private:
        void calc_step_();

        Eigen::MatrixXd create_tmp_matrix_();

        void init_butcher_();

        RecalcFuncType recalc_func_;
        ResultFuncType result_func_;
        double max_diff_;
        double min_diff_;

        double time_;
        double step_;
        Eigen::VectorXd values_;

        struct Result res_;

        static constexpr double MAX_TIME = 0.2;
        Eigen::VectorXd STEPS;
        Eigen::MatrixXd B;
        Eigen::MatrixXd A;
    };

    class DPSolver : public Solver {
    public:
        struct Result {
            double min_step;
            int total_step;
        };

    public:
        DPSolver(RecalcFuncType &recalc_func, ResultFuncType &result_func, const SolverOptions &opt)
                : recalc_func_(recalc_func), result_func_(result_func), time_(opt.start_time), step_(opt.step),
                  values_(opt.start_values), max_diff_(opt.max_diff), min_diff_(opt.min_diff) {
            init_butcher_();
        };

        void solve() override;

        Result result() { return res_; }

    private:
        void calc_step_();

        Eigen::MatrixXd create_tmp_mat_();

        void init_butcher_();

    private:
        RecalcFuncType recalc_func_;
        ResultFuncType result_func_;
        double max_diff_;
        double min_diff_;

        double time_;
        double step_;
        Eigen::VectorXd values_;

        struct Result res_;

    private:
        static constexpr double MAX_TIME = 10;
        Eigen::VectorXd STEPS;
        std::vector<Eigen::MatrixXd> B;
        Eigen::MatrixXd A;
    };
}