#include "solver.h"

namespace hw_cv {
    void RungeSolver::solve() {
        double max_diff = 0;
        while (time_ < MAX_TIME) {
            calc_step_();
            double current_diff = (values_ - result_func_(time_)).cwiseAbs().maxCoeff();

            if (current_diff > max_diff) {
                max_diff = current_diff;
            }

            if (current_diff > DIFF_STEP) {
                step_ /= 2;
                max_diff = 0;
                values_ = result_func_(time_);
            }
        }
        res_ = {max_diff, step_};
    }

    void RungeSolver::calc_step_() {
        Eigen::MatrixXd tmp = create_tmp_matrix_();

        Eigen::VectorXd x = (B * tmp).transpose();

        values_ += x * step_;
        time_ += step_;
    }

    Eigen::MatrixXd RungeSolver::create_tmp_matrix_() {
        Eigen::MatrixXd tmp(A.cols(), values_.size());
        Eigen::VectorXd val = values_;
        tmp.row(0) = recalc_func_(time_, val);

        for (int i = 1; i < tmp.rows(); ++i) {
            val = values_;
            for (int j = 0; j < i; ++j) {
                val += tmp.row(j) * A(i, j) * step_;
            }
            tmp.row(i) = recalc_func_(time_ + STEPS(i) * step_, val);
        }

        return tmp;
    }

    void RungeSolver::init_butcher_() {
        Eigen::MatrixXd butcher_mat(5, 5);
        butcher_mat << 0, 0, 0, 0, 0, 1.0 / 2, 1.0 / 2, 0, 0, 0, 1.0 / 2, 0, 1.0 / 2, 0, 0, 1.0, 0, 0, 1.0, 0, 0, 1.0 /
                                                                                                                  6,
                1.0 / 3, 1.0 / 3, 1.0 / 6;
        STEPS = butcher_mat.block(0, 0, butcher_mat.cols() - 1, 1);
        B = butcher_mat.block(butcher_mat.rows() - 1, 1, 1, butcher_mat.cols() - 1);
        A = butcher_mat.block(0, 1, butcher_mat.cols() - 1, butcher_mat.cols() - 1);
    }

    void DPSolver::solve() {
        double min_step = DEFAULT_STEP;
        int total_steps = 0;
        while (time_ < MAX_TIME) {
            ++total_steps;
            calc_step_();

            if (step_ < min_step) {
                min_step = step_;
            }
        }
        res_ = {min_step, total_steps};
    }

    void DPSolver::calc_step_() {
        Eigen::VectorXd x1;
        Eigen::VectorXd x2;
        Eigen::MatrixXd tmp;
        double diff;
        do {
            tmp = create_tmp_mat_();

            x1 = (B[0] * tmp).transpose();
            x2 = (B[1] * tmp).transpose();

            diff = (x1 - x2).cwiseAbs().maxCoeff();
            if (diff > max_diff_) {
                step_ /= 2;
            } else if (diff < min_diff_) {
                step_ *= 2;
            }
        } while (diff > max_diff_);

        values_ += x1 * step_;
        time_ += step_;
    }

    Eigen::MatrixXd DPSolver::create_tmp_mat_() {
        Eigen::MatrixXd tmp(A.cols(), values_.size());
        Eigen::VectorXd val = values_;
        tmp.row(0) = recalc_func_(time_, val);

        for (int i = 1; i < tmp.rows(); ++i) {
            val = values_;
            for (int j = 0; j < i; ++j) {
                val += tmp.row(j) * A(i, j) * step_;
            }
            tmp.row(i) = recalc_func_(time_ + STEPS(i) * step_, val);
        }

        return tmp;
    }

    void DPSolver::init_butcher_() {
        Eigen::MatrixXd butcher_mat(9, 8);
        butcher_mat << 0, 0, 0, 0, 0, 0, 0, 0, 1.0 / 5, 1.0 / 5, 0, 0, 0, 0, 0, 0, 3.0 / 10, 3.0 / 40, 9.0 /
                                                                                                       40, 0, 0, 0, 0,
                0, 4.0 / 5, 44.0 / 45, -56.0 / 15, 32.0 / 9, 0, 0, 0, 0, 8.0 / 9, 19372.0 / 6561, -25360.0 / 2187,
                64448.0 / 6561, -212.0 / 729, 0, 0, 0, 1, 9017.0 / 3168, -355.0 / 33, 46732.0 / 5247, 49.0 / 176,
                -5103.0 / 18656, 0, 0, 1, 35.0 / 384, 0, 500.0 / 1113, 125.0 / 192, -2187.0 / 6784, 11.0 / 84, 0, 0,
                35.0 / 384,
                0, 500.0 / 1113, 125.0 / 192, -2187.0 / 6784, 11.0 / 84, 0, 0, 5179.0 / 57600, 0, 7571.0 / 16695,
                393.0 / 640,
                -92097.0 / 339200, 187.0 / 2100, 1.0 / 40;
        B = {butcher_mat.block(butcher_mat.rows() - 2, 1, 1, butcher_mat.cols() - 1),
             butcher_mat.block(butcher_mat.rows() - 1, 1, 1, butcher_mat.cols() - 1)};
        A = butcher_mat.block(0, 1, butcher_mat.cols() - 1, butcher_mat.cols() - 1);
        STEPS = butcher_mat.block(0, 0, butcher_mat.cols() - 1, 1);
    }
}