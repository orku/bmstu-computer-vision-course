#include "solver.h"

#include <gtest/gtest.h>

TEST(DIFF_EQUATION_TEST, __COUNTER__) {
    const double a = -2;
    const double b = 0.2;
    const double d = 1;

    hw_cv::RecalcFuncType recalc_func = [a, b](double time, const Eigen::VectorXd &val) -> Eigen::VectorXd {
        Eigen::VectorXd ret(1);
        ret(0) = a * time - b * val(0, 0);
        return ret;
    };

    hw_cv::ResultFuncType result_func = [a, b](double time) -> Eigen::VectorXd {
        Eigen::VectorXd result(1);
        result(0) = a / b * (time - 1 / b) + 0.57 * exp(-b * time);
        return result;
    };

    Eigen::VectorXd init_value(1, 1);
    init_value(0, 0) = d;

    hw_cv::SolverOptions opt{
            .step = hw_cv::DEFAULT_STEP,
            .max_diff = 10e-10,
            .min_diff = 10e-10,
            .start_time = 0,
            .start_values = init_value,
    };

    hw_cv::RungeSolver runge_solver(recalc_func, result_func, opt);
    runge_solver.solve();
    auto runge_res = runge_solver.result();
    hw_cv::DPSolver dp_solver(recalc_func, result_func, opt);
    dp_solver.solve();
    auto dp_res = dp_solver.result();

    std::cout << "min_step (DP) : " << dp_res.min_step << std::endl;
    std::cout << "total_steps (DP) : " << dp_res.total_step << std::endl;
    std::cout << "max_diff (Runge) : " << runge_res.max_diff << std::endl;
    std::cout << "step (Runge) : " << runge_res.step << std::endl << std::endl;
}

TEST(DIFF_EQUATION_TEST, __COUNTER__) {
    hw_cv::RecalcFuncType recalc_func = [](double time, const Eigen::VectorXd &val) -> Eigen::VectorXd {
        Eigen::VectorXd ret(2, 1);
        ret(0, 0) = 9 * val(0, 0) + 24 * val(1, 0) + 5 * cos(time) - 1.0 / 3 * sin(time);
        ret(1, 0) = -24 * val(0, 0) - 51 * val(1, 0) + 9 * cos(time) + 1.0 / 3 * sin(time);
        return ret;
    };

    hw_cv::ResultFuncType result_func = [](double time) -> Eigen::VectorXd {
        Eigen::VectorXd result(2);
        result(0) = 2.0 * exp(-3 * time) - exp(-39 * time) + 1.0 / 3 * cos(time);
        result(1) = -exp(-3 * time) + 2.0 * exp(-39 * time) - 1.0 / 3 * cos(time);
        return result;
    };

    Eigen::VectorXd init_value(2, 1);
    init_value(0, 0) = 4.0 / 3;
    init_value(1, 0) = 2.0 / 3;

    hw_cv::SolverOptions opt{
            .step = hw_cv::DEFAULT_STEP,
            .max_diff = 10e-10,
            .min_diff = 10e-10,
            .start_time = 0,
            .start_values = init_value,
    };

    hw_cv::RungeSolver runge_solver(recalc_func, result_func, opt);
    runge_solver.solve();
    auto runge_res = runge_solver.result();
    hw_cv::DPSolver dp_solver(recalc_func, result_func, opt);
    dp_solver.solve();
    auto dp_res = dp_solver.result();

    std::cout << "min_step (DP) : " << dp_res.min_step << std::endl;
    std::cout << "total_steps (DP) : " << dp_res.total_step << std::endl;
    std::cout << "max_diff (Runge) : " << runge_res.max_diff << std::endl;
    std::cout << "step (Runge) : " << runge_res.step << std::endl << std::endl;
}