#include <Matrix.h>
#include <gtest/gtest.h>

TEST(matrix, LU) {
    hw_cv::Matrix mat({{1, 1, 1}, {2, 3, 5}, {4, 6, 8}});
    auto [l, u] = mat.LU();
    hw_cv::Matrix expected_l({{1, 0, 0}, {2, 1, 0}, {4, 2, 1}});
    hw_cv::Matrix expected_u({{1, 1, 1}, {0, 1, 3}, {0, 0, -2}});

    ASSERT_EQ(expected_l, l);
    ASSERT_EQ(expected_u, u);
    ASSERT_EQ(l.dot(u), mat);
}

TEST(matrix, solve1) {
    hw_cv::Matrix A({{1, 2, 3}, {3, 5, 7}, {1, 3, 4}});
    hw_cv::Matrix b({{3}, {0}, {1}});

    hw_cv::Matrix x = hw_cv::solve(A, b);

    hw_cv::Matrix expected_x({{-4}, {-13}, {11}});

    ASSERT_EQ(x, expected_x);
}

TEST(matrix, solve2) {  // from task
    hw_cv::Matrix A({{10, 6, 2, 0}, {5, 1, -2, 4}, {3, 5, 1, -1}, {0, 6, -2, 2}});
    hw_cv::Matrix b({{25}, {14}, {10}, {8}});

    hw_cv::Matrix x = hw_cv::solve(A, b);

    hw_cv::Matrix expected_x({{2}, {1}, {-0.5}, {0.5}});

    ASSERT_EQ(x, expected_x);
}

TEST(matrix, det) {  // from task
    hw_cv::Matrix A({{10, 6, 2, 0}, {5, 1, -2, 4}, {3, 5, 1, -1}, {0, 6, -2, 2}});
    ASSERT_EQ(A.det(), 44);
}

TEST(matrix, inverse) {  // from task
    hw_cv::Matrix A({{10, 6, 2, 0}, {5, 1, -2, 4}, {3, 5, 1, -1}, {0, 6, -2, 2}});
    hw_cv::Matrix expected_inv({{-8. / 11, 8. / 11, 17. / 11, -15. / 22},
                             {3. / 11, -3. / 11, -5. / 11, 7. / 22},
                             {73. / 22, -31. / 11, -70. / 11, 27. / 11},
                             {5. / 2, -2., -5., 2.}});
    ASSERT_EQ(A.inverse(), expected_inv);
}

