# bmstu-computer-vision-course

Homework for Computer Vision  course in BMSTU   

[![pipeline status](https://gitlab.com/orku/bmstu-computer-vision-course/badges/main/pipeline.svg)](https://gitlab.com/orku/bmstu-computer-vision-course/-/commits/main)

## Vision System BMSTU Homework

### Task №1

### Library for simple matrix operations

* [Realization](https://gitlab.com/orku/bmstu-computer-vision-course/-/tree/main/tasks/task_1)
* [Tests](https://gitlab.com/orku/bmstu-computer-vision-course/-/blob/main/tasks/test/matrix_test.cpp)

### Task №2

### Simple CLI resistor resistance calculator

* [Executable](https://gitlab.com/orku/bmstu-computer-vision-course/-/tree/main/tasks/task_2)

### Task №3

### Differential equation solver

* [Realization](https://gitlab.com/orku/bmstu-computer-vision-course/-/tree/main/tasks/task_3)
* [Tests](https://gitlab.com/orku/bmstu-computer-vision-course/-/blob/main/tasks/test/diff_equations_test.cpp)

### Task 4

### Feature matching

* [Realization](https://gitlab.com/orku/bmstu-computer-vision-course/-/tree/main/tasks/task_4)
* [Executable](https://gitlab.com/orku/bmstu-computer-vision-course/-/tree/main/tasks/task_4/main.cpp)

### Task 5

### Text detection

* [Realization](https://gitlab.com/orku/bmstu-computer-vision-course/-/tree/main/tasks/task_5)
* [Executable](https://gitlab.com/orku/bmstu-computer-vision-course/-/tree/main/tasks/task_5/main.cpp)